﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RTLShowApi;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System.Net;
using RTLShowApi.Entity;
using Newtonsoft.Json;

namespace RTLShowCastAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShowsController : ControllerBase
    {
        // GET api/values
      


        [Route("api/[controller]/{page?}")]
        [HttpGet("{page?}")]
        public ActionResult<string> Get(int page)
        {
             using (var context = new RTLShowApiDbContext())
            {
                new ScrapManager().ScrapAndStoreShowInfo(context);
                 context.Database.EnsureCreated();

                var TaskforList= context.Shows.AsNoTracking().Include(Show=> Show.ShowCast).ThenInclude(sc => sc.Cast).ToListAsync();
                if(TaskforList.IsCompleted)
                {

                var setting = new JsonSerializerSettings
                {
                    Formatting = Newtonsoft.Json.Formatting.Indented,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                };
               
                    var showCastList = from t in TaskforList.Result select new { 
                        Id=t.Id,
                        Name = t.Name,
                        Cast= t.ShowCast.Select(s => s.Cast).OrderByDescending(c => c.Birthday).ToList()
                          };

                if(page >= 1)
                {
                    var numberOfObjectsPerPage = 10;
                    var skipItem=(numberOfObjectsPerPage * (page-1));
                    
                      return JsonConvert.SerializeObject(showCastList.Skip(skipItem).Take(numberOfObjectsPerPage),setting);
                }
                 return JsonConvert.SerializeObject(showCastList,setting);
                }
                else
                return "No Info Avaliable";
            }
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
