using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RTLShowApi;
using RTLShowApi.Entity;

namespace  RTLShowCastAPI
{
    public class ScrapManager
    {
        public void ScrapAndStoreShowInfo(RTLShowApiDbContext context)
        {
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("http://api.tvmaze.com/shows");

                List<Show> shows = new List<Show>();
                shows = JsonConvert.DeserializeObject<List<Show>>(json);
                foreach (var show in shows)
                {

                    var showResultTask = context.Shows.AnyAsync(c => c.Id == show.Id);
                    if (showResultTask.IsCompleted)
                    {
                        if (!showResultTask.Result)
                        {
                            context.Shows.Add(show);

                            using (WebClient wc2 = new WebClient())
                            {
                                var castJson = wc2.DownloadString("http://api.tvmaze.com/shows/" + show.Id + "/cast");
                                var castList = JsonConvert.DeserializeObject<List<CastObject>>(castJson);
                                foreach (var castObj in castList)
                                {
                                    var resultTask = context.Casts.AnyAsync(c => c.Id == castObj.person.Id);
                                    if (resultTask.IsCompleted)
                                    {
                                        if (!resultTask.Result)
                                        {
                                            context.Casts.Add(castObj.person);
                                            context.Add(new ShowCast() { Show = show, Cast = castObj.person });
                                            context.SaveChanges();
                                        }
                                    }

                                }
                            }
                            Task.Delay(200).Wait();

                        }
                    }
                }
            }

        }
    }
}