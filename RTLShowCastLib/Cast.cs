using System.Collections.Generic;
using Newtonsoft.Json;

namespace RTLShowApi.Entity
{
    public class Cast
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Birthday { get; set; }

         [JsonIgnore]
        public ICollection<ShowCast> ShowCast { get; set; }
    }

}
