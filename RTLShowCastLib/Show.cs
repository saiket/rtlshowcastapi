using System.Collections.Generic;
using Newtonsoft.Json;
namespace RTLShowApi.Entity
{
        public class Show
    {
        public int Id { get; set; }

      public string Name { get; set; }

        public ICollection<ShowCast> ShowCast { get; set; }
        
    }
    
}