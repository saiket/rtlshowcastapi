using Microsoft.EntityFrameworkCore;
using RTLShowApi.Entity;

namespace RTLShowApi
{

    public class RTLShowApiDbContext : DbContext
    {
        public DbSet<Show> Shows { get; set; }
        public DbSet<Cast> Casts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=RTLShowInfoDB");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Show>().HasKey(t => t.Id);
            modelBuilder.Entity<Show>().Property(t => t.Id).ValueGeneratedNever();
            
            modelBuilder.Entity<Cast>().HasKey(t => t.Id);
            modelBuilder.Entity<Cast>().Property(t => t.Id).ValueGeneratedNever();          

            modelBuilder.Entity<ShowCast>()
                .HasKey(sc => new { sc.ShowId, sc.CastId });

            modelBuilder.Entity<ShowCast>()
                .HasOne(sc => sc.Show)
                .WithMany(s => s.ShowCast)
                .HasForeignKey(sc => sc.ShowId);

            modelBuilder.Entity<ShowCast>()
                .HasOne(bc => bc.Cast)
                .WithMany(c => c.ShowCast)
                .HasForeignKey(sc => sc.CastId);
        }
    }
}